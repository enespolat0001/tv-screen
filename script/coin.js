let btc = document.getElementById("btc")
let btcUSD = document.getElementById("btcUSD")
let eth = document.getElementById("eth")
let ethUSD = document.getElementById("ethUSD")
let bnb = document.getElementById("bnb")
let bnbUSD = document.getElementById("bnbUSD")
let ubxs = document.getElementById("ubxs")
let ubxsUSD = document.getElementById("ubxsUSD")



function coin() {
    fetch("coin.json")
      .then(response => response.json())
      .then(coinData => {
        console.log(coinData)
        let oldCryptoBTCUSD = localStorage.getItem("btc-crypto")
       cryptoBTCUSD = parseInt(coinData.data.BTC.quote.USD.price);
       btcUSD.innerHTML = cryptoBTCUSD
       btc.innerHTML = coinData.data.BTC.symbol

       localStorage.setItem("btc-crypto",coinData.data.BTC.quote.USD.price)
       if(oldCryptoBTCUSD > coinData.data.BTC.quote.USD.price){
           btcUSD.className = "text-red-500"
           btc.className = "text-red-500"
           btcUSD.style.display = "flex"
           btcUSD.innerHTML = cryptoBTCUSD + "<img class='ml-5 ' src='/images/svg/down.svg' alt=''>"
       }else{
        btcUSD.className = "text-green-500"
        btc.className = "text-green-500"
        btcUSD.style.display = "flex"
        btcUSD.innerHTML = cryptoBTCUSD + "<img class='ml-5 ' src='/images/svg/up.svg' alt=''>"
       }


       let oldCryptoETHUSD = localStorage.getItem("eth-crypto")
       cryptoETHUSD = parseInt(coinData.data.ETH.quote.USD.price);
       ethUSD.innerHTML = cryptoETHUSD
       eth.innerHTML = coinData.data.ETH.symbol

       localStorage.setItem("eth-crypto",coinData.data.ETH.quote.USD.price)
       if(oldCryptoETHUSD > coinData.data.ETH.quote.USD.price){
           ethUSD.className = "text-red-500"
           eth.className = "text-red-500"
           bnbUSD.style.display = "flex"
           ethUSD.innerHTML = cryptoETHUSD + "<img class='ml-5 ' src='/images/svg/down.svg' alt=''>"
       }else{
        ethUSD.className = "text-green-500"
        eth.className = "text-green-500"
        ethUSD.style.display = "flex"
        ethUSD.innerHTML = cryptoETHUSD + "<img class='ml-5 ' src='/images/svg/up.svg' alt=''>"
       }



       let oldCryptoBNBUSD = localStorage.getItem("btc-crypto")
       cryptoBNBUSD = parseInt(coinData.data.BNB.quote.USD.price);
       bnbUSD.innerHTML = cryptoBNBUSD
       bnb.innerHTML = coinData.data.BNB.symbol

       localStorage.setItem("bnb-crypto",coinData.data.BNB.quote.USD.price)
       if(oldCryptoBNBUSD > coinData.data.BNB.quote.USD.price){
           bnbUSD.className = "text-red-500"
           bnb.className = "text-red-500"
           bnbUSD.style.display = "flex"
           bnbUSD.innerHTML = cryptoBNBUSD + "<img class='ml-5 ' src='/images/svg/down.svg' alt=''>"
       }else{
        bnbUSD.className = "text-green-500"
        bnb.className = "text-green-500"
        bnbUSD.style.display = "flex"
        bnbUSD.innerHTML = cryptoBNBUSD + "<img class='ml-5 ' src='/images/svg/up.svg' alt=''>"
       }



       let oldCryptoUBXUSD = localStorage.getItem("ubxs-crypto")
       cryptoUBXSUSD = parseInt(coinData.data.UBXS.quote.USD.price);
       ubxsUSD.innerHTML = cryptoUBXSUSD
       ubxs.innerHTML = coinData.data.UBXS.symbol

       localStorage.setItem("ubxs-crypto",coinData.data.UBXS.quote.USD.price)
       if(oldCryptoUBXUSD > coinData.data.UBXS.quote.USD.price){
           ubxsUSD.className = "text-red-500"
           ubxs.className = "text-red-500"
           ubxsUSD.style.display = "flex"
           ubxsUSD.innerHTML = cryptoUBXSUSD + "<img class='ml-5 ' src='/images/svg/down.svg' alt=''>"
       }else{
        ubxsUSD.className = "text-green-500"
        ubxs.className = "text-green-500"
        ubxsUSD.style.display = "flex"
        ubxsUSD.innerHTML = cryptoUBXSUSD + "<img class='ml-5 ' src='/images/svg/up.svg' alt=''>"
       }
      })
      .catch((error) => {
        console.error('Error:', error);
      });;
}

coin()