let tTitle = document.getElementById("topTitle")
let tTitleContent = document.getElementById("topTitleContent")
let mTitle = document.getElementById("midTitle")
let mTitleContent = document.getElementById("midTitleContent")
let bTitle = document.getElementById("botTitle")

fetch("content.json")
.then(response => response.json())
.then(contentData => {
    tTitle.innerHTML = contentData.contentNews[0].topTitle
    tTitleContent.innerHTML = contentData.contentNews[1].topTitleContent
    mTitle.innerHTML = contentData.contentNews[2].midTitle
    mTitleContent.innerHTML = contentData.contentNews[3].midTitleContent
    bTitle.innerHTML = contentData.contentNews[4].botTitle

  }).catch((error) => {
    console.error('Error:', error);
  });;