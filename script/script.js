let locationWeather = document.getElementById("location")
let celsius = document.getElementById("celsius")
let weatherImg = document.getElementById("weatherImg")
let keyframeContent = document.getElementById("keyframeContent")


let locationsWeather = ['https://api.openweathermap.org/data/2.5/weather?q=istanbul&units=metric&appid=7e4dbb30a0c282f5cfdd7c6e10cd3538',
  'https://api.openweathermap.org/data/2.5/weather?q=delaware&units=metric&appid=7e4dbb30a0c282f5cfdd7c6e10cd3538']


function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

async function checkWeatherAPI() {
  for (var i = 0; ; i++) {
    i = i % locationsWeather.length

    fetch(locationsWeather[i])
      .then(response => response.json())
      .then(weatherData => {
        locationWeather.innerHTML = weatherData.name
        celsius.innerHTML = weatherData.main.temp
        weatherImg.src = "images/" + weatherData.weather[0].icon + ".png"

      })
      .catch((error) => {
        console.error('Error:', error);
      });;
    await sleep(5000);
  }
}

checkWeatherAPI();

for (let i = 0; i < "keyframe-content.json".length; i++) {
  
  fetch("keyframe-content.json")
  .then(response => response.json())
  .then(data => {
    keyframeContent.innerHTML += "<img class='ml-10 ' src='/images/svg/ubxs-white.svg' alt=''>" + data.content[i].title  
    keyframeContent.style.display = "flex"
  }).catch((error) => {
    console.error('Error:', error);
  });;
}




